import Vue from 'vue'
import { Button, Container, Main, Image } from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/index.css';
import animate from 'animate.css'
import 'font-awesome/css/font-awesome.min.css'

import App from './App.vue'


Vue.use(Button);
Vue.use(Container);
Vue.use(Main);
Vue.use(Image);
Vue.use(animate);


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
